package astar;

import java.util.HashMap;
import java.util.PriorityQueue;

/**
 * Generic implementation of the A* algorithm.
 * 
 * @param <S> the class representing states of the problem to solve
 */
public class AStar<S extends State<S>>  {

    /**
     * Finds the shortest path to a goal state, starting from `start`.
     * 
     * Note that `heuristics` might be provided as a lambda expression (see examples).
     */
    public static <T extends State<T>> Result<T> solve(T start, Heuristics<T> heuristics) {
        return new AStar<T>().do_solve(start, heuristics);
    }

    // Private implementation

    HashMap<String, Visited> visited = null;
    PriorityQueue<Visited> frontier = null;

    Result<S> do_solve(S start, Heuristics<S> heuristics) {
        assert visited == null;
        debug("starting A*");
        visited = new HashMap<>();
        frontier = new PriorityQueue<>();
        int visits = 0;
        int closed = 0;
        Visited sv = new Visited(start, null, 0, heuristics.compute(start));
        visited.put(start.toString(), sv);
        frontier.add(sv);
        while(hasCandidate()) {
            Visited c = frontier.poll();
            c.closed = true;
            closed += 1;
            debug("closing " + c.state.toString() + "\t(" + c.estCost + "; " + c.actCost + ")");
            if (c.state.isGoal()) {
                return makeResult(c, closed, visits);
            }
            for (Transition<S> e: c.state.transitions()) {
                Visited ev = visited.get(e.destState.toString());
                if (ev != null && ev.closed) { continue; }
                visits += 1;
                double actCost = c.actCost + e.cost;
                double estCost;
                if (ev != null) {
                    double deltaCost = actCost - ev.actCost;
                    if (deltaCost >= 0) {
                        continue;
                    } else {
                        ev.obsolete = true;
                        estCost = ev.estCost - deltaCost; // avoid callinh heuristics again
                    }
                } else {
                    estCost = actCost + heuristics.compute(e.destState);
                }             
                ev = new Visited(e.destState, c, actCost, estCost);
                visited.put(e.destState.toString(), ev);
                frontier.add(ev);
            }
        }
        return null;
    }

    boolean hasCandidate() {
        // TODO this is not optimal;
        // it would be better to replace PropertyQueue by a queue that supports updating,
        // such as Fibonacci heap or Pairing heap
        while(frontier.peek() != null) {
            if (frontier.peek().obsolete) {
                frontier.poll();
            } else {
                return true;
            }
        }
        return false;
    }

    Result<S> makeResult(Visited goal, int closed, int visits) {
        Result<S> r = new Result<>();
        r.totalCost = goal.actCost;
        r.closed = closed;
        r.discovered = visited.size();
        r.visits = visits;
        Visited v = goal;
        while(v.parent != null) {
            r.path.addFirst(new Transition<>(v.state, v.actCost - v.parent.actCost));
            v = v.parent;
        }
        r.path.addFirst(new Transition<>(v.state, 0));

        return r;
    }

    class Visited implements Comparable<Visited> {
        S state;        
        Visited parent;
        double actCost;
        double estCost;
        boolean obsolete = false;
        boolean closed = false;

        Visited(S state, Visited parent, double actCost, double estCost) {
            this.state = state;
            this.parent = parent;
            this.actCost = actCost;
            this.estCost = estCost;
        }

        public int compareTo(Visited other) {
            if (estCost < other.estCost) {
                return -1;
            } else if (estCost == other.estCost) {
                return 0;
            } else {
                return 1;
            }
        }
    }

    boolean DEBUG = System.getenv("ASTAR_DEBUG") != null;

    void debug(String msg) {
        if (DEBUG) {
            System.err.println(msg);
        }
    }
}