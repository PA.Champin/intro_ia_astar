package astar;

/**
 * One state in a problem space that can be explored with the A* algorithm,
 * via `AStar.solve`.
 * 
 * @param <N> the type of neighbours state; must in general be the implementing type itself.
 */
public interface State<N extends State<N>> {

    /**
     * String representation of this state.
     * 
     * This is used for both debugging AND comparing two instances.
     * Therefore, it is important that two different states have different String representations.
     */
    String toString();

    /**
     * Return all possible transitions (target state + cost) from this state.
     */
    Iterable<Transition<N>> transitions();

    /**
     * Indicates whether this state is a goal state.
     */
    boolean isGoal();
}
