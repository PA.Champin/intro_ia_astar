package astar;

/**
 * A transition toward a given state, with an associated cost.
 * 
 * @param <S> the class representing states of the problem to solve
 */
public class Transition<S extends State> {

    /**
     * The target state of this transition.
     */
    public S destState;
    /**
     * The cost of this transition.
     */
    public double cost;

    public Transition(S destState, double cost) {
        this.destState = destState;
        this.cost = cost;
    }
}
