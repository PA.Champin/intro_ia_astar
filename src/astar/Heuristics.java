package astar;

/**
 * The interface used to describe the heuristics in static method `AStar.solve`;
 * usually described simply as a lambda expression.
*/
public interface Heuristics<T extends State<T>> {
    double compute(T state);
};

