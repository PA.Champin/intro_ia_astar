package astar;

import java.util.LinkedList;;

/**
 * The result of a shortest-path search with A*.
 * 
 * @param <S> the class representing states of the problem to solve
 */
public class Result<S extends State<S>>  {
    /**
     * The shortest path found by A*, as a list of transitions.
     */
    public LinkedList<Transition<S>> path = new LinkedList<>();
    /**
     * The total cost of `path`.
     */
    public double totalCost= 0;
    /**
     * The number of states that were closed during the search.
     */
    public int closed = 0;
    /**
     * The number of states that were discovered during the search.
     * As all closed states must first be discovered,
     * this is greater or equal to `closed`.
     */
    public int discovered = 0;
    /**
     * The number of visit on discovered states during the search.
     * As some discovered states may be visited several times
     * (coming from different neighbours),
     * this is greater or equal to `discovered`.
     */
    public int visits = 0;

    /**
     * Prints on stdout a detailed report of this result.
     */
    public void printReport() {
        for (Transition<S> e: path) {
            System.out.println(e.destState.toString() + "\t(" + e.cost + ")");
        }
        System.out.println("total cost: " + totalCost);
        printShortReport();
    }

    /**
     * Prints on stdout a summarized report of this result.
     */
    public void printShortReport() {
        System.out.println("visited: " + visits + " (" + discovered + " unique, ratio: " + visits*1.0/discovered + ")");
        System.out.println("closed:  " + closed);
    }
}