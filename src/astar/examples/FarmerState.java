package astar.examples;

import java.util.ArrayList;
import astar.*;

/**
 * https://en.wikipedia.org/wiki/Fox,_goose_and_bag_of_beans_puzzle
 */
public class FarmerState implements State<FarmerState> {

    // Structure of the state

    static int FARMER = 0;
    static int CABBAGE = 1;
    static int GOAT = 2;
    static int WOLF = 3;

    static boolean LEFT = false;
    static boolean RIGHT = true;

    boolean[] position;

    private FarmerState(boolean position[]) {
        this.position = position;
    }

    public static FarmerState start() {
        return new FarmerState(new boolean[] {LEFT, LEFT, LEFT, LEFT});
    }

    // Implementation of the 'State' interface

    public String toString() {
        String ret = "";
        for(int i=0; i<4; i+=1) {
            if (position[i] == LEFT) ret += i; else ret += '_';
        }
        ret += " | ";
        for(int i=0; i<4; i+=1) {
            if (position[i] == RIGHT) ret += i; else ret += '_';
        }
        return ret;
    }

    public Iterable<Transition<FarmerState>> transitions() {
        ArrayList<Transition<FarmerState>> ret = new ArrayList<>(4);
        if (position[FARMER] == position[CABBAGE]) {
            // move cabbage only if GOAT and WOLF won't be left alone
            if (position[GOAT] != position[WOLF]) {
                boolean pos[] = position.clone();
                pos[FARMER] = !pos[FARMER];
                pos[CABBAGE] = !pos[CABBAGE];
                ret.add(new Transition<>(new FarmerState(pos), 1));
            } 
        }
        if (position[FARMER] == position[WOLF]) {
            // move cabbage only if CABBAGE and GOAT won't be left alone
            if (position[CABBAGE] != position[GOAT]) {
                boolean pos[] = position.clone();
                pos[FARMER] = !pos[FARMER];
                pos[WOLF] = !pos[WOLF];
                ret.add(new Transition<>(new FarmerState(pos), 1));
            } 
        }
        if (position[FARMER] == position[GOAT]) {
            boolean pos[] = position.clone();
            pos[FARMER] = !pos[FARMER];
            pos[GOAT] = !pos[GOAT];
            ret.add(new Transition<>(new FarmerState(pos), 1));
        }
        if (position[CABBAGE] != position[GOAT] && position[GOAT] != position[WOLF]
        || position[CABBAGE] == position[WOLF]) {
            boolean pos[] = position.clone();
            pos[FARMER] = !pos[FARMER];
            ret.add(new Transition<>(new FarmerState(pos), 1));
        }
        return ret;
    }

    public boolean isGoal() {
        return position[FARMER] == RIGHT
            && position[CABBAGE] == RIGHT
            && position[GOAT] == RIGHT
            && position[WOLF] == RIGHT;
    }

    // Heuristics

    public double h1() {
            int ret = 0;
            if (position[CABBAGE] == LEFT) { ret += 2; }
            if (position[GOAT]    == LEFT) { ret += 2; }
            if (position[WOLF]    == LEFT) { ret += 2; }
            if (position[FARMER]  == LEFT && ret > 0) { ret -= 1; }
            return ret;
    }

    // Test

    public static void main(String args[]) {
        Result res = AStar.solve(FarmerState.start(), (s) -> s.h1());
        res.printReport();

        System.out.println("-- Dijkstra:");
        res = AStar.solve(FarmerState.start(), (s) -> 0);
        res.printShortReport();
    }
}
