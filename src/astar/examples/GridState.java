package astar.examples;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.sqrt;
import java.util.ArrayList;
import astar.*;

/**
 * Finds the shortest path from any point to 0:0 on a grid.
 */
public class GridState implements State<GridState> {

    int x, y;

    private GridState(int x, int y) {
        this.x = x; this.y = y;
    }

    public static GridState start(int x, int y) {
        return new GridState(x, y);
    }

    // State implementation

    public String toString() {
        return ""+x+":"+y;
    }

    public Iterable<Transition<GridState>> transitions() {
        ArrayList<Transition<GridState>> ret = new ArrayList<>(8);
        ret.add(new Transition<>(new GridState(x+1, y+0), 1));
        ret.add(new Transition<>(new GridState(x+1, y-1), sqrt(2)));
        ret.add(new Transition<>(new GridState(x+0, y-1), 1));
        ret.add(new Transition<>(new GridState(x-1, y-1), sqrt(2)));
        ret.add(new Transition<>(new GridState(x-1, y+0), 1));
        ret.add(new Transition<>(new GridState(x-1, y+1), sqrt(2)));
        ret.add(new Transition<>(new GridState(x+0, y+1), 1));
        ret.add(new Transition<>(new GridState(x+1, y+1), sqrt(2)));
        return ret;
    }

    public boolean isGoal() {
        return x == 0 && y == 0;
    }

    // Heuristics

    public double euclidean() {
        return sqrt(x*x + y*y);
    }

    public double maxdim() {
        return max(abs(x), abs(y));
    }

    // Test

    public static void main(String args[]) {
        int x = 4, y = 5;
        if (args.length >= 2) {
            x = Integer.parseInt(args[0]);
            y = Integer.parseInt(args[1]);
        }
        GridState start = GridState.start(x, y);

        System.out.println("-- Heuristics: Euclidean:");
        Result<GridState> res = AStar.solve(
            start,
            new Heuristics<GridState>() {
                public double compute(GridState s) {
                    return s.euclidean();
                }
            }
        );
        res.printReport();

        System.out.println("-- Heuristics: MaxDim:");
        res = AStar.solve(start, (s) -> s.maxdim());
        res.printShortReport();

        System.out.println("-- Dijkstra:");
        res = AStar.solve(start, (s) -> 0);
        res.printShortReport();
    }
}
